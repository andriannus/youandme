<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => 'cors'], function() {
	Route::resource('activity', 'ActivityController', [
		'except' => ['create', 'edit']
	]);

	Route::group(['prefix' => 'user'], function() {
		Route::get('/', [
			'uses' => 'UserController@index'
		]);

		Route::get('/search', [
			'uses' => 'UserController@search'
		]);

		Route::get('/{user}', [
			'uses' => 'UserController@show'
		]);

		Route::patch('/{user}', [
			'uses' => 'UserController@update'
		]);

		Route::post('/{user}/oldpass', [
			'uses' => 'UserController@oldpass'
		]);

		Route::patch('/{user}/updatepass', [
			'uses' => 'UserController@updatePass'
		]);

		Route::post('/{user}/report', [
			'uses' => 'UserController@report'
		]);

		Route::get('/{user}/activities', [
			'uses' => 'UserController@activities'
		]);
	});

	Route::group(['prefix' => 'auth'], function() {
		Route::post('/register', [
			'uses' => 'AuthController@store'
		]);

		Route::post('/login', [
			'uses' => 'AuthController@login'
		]);

		Route::post('/checkmail', [
			'uses' => 'AuthController@checkMail'
		]);

		Route::post('/verify', [
			'uses' => 'AuthController@verify'
		]);
		
		Route::get('/active', [
			'uses' => 'AuthController@active'
		]);

		Route::post('/resend', [
			'uses' => 'AuthController@resend'
		]);

		Route::group(['middleware' => ['jwt.auth', 'jwt.refresh']], function() {
			Route::post('/current', [
				'uses' => 'AuthController@current'
			]);
	
			Route::post('/logout', [
				'uses' => 'AuthController@logout'
			]);
		});
	});
});