<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('avatar')->default('avatar.jpeg');
            $table->enum('gender', ['l', 'p'])->nullable();
            $table->date('birthday')->nullable();
            $table->text('description')->nullable();
            $table->string('profession')->nullable();
            $table->string('agency')->nullable();
            $table->text('address')->nullable();
            $table->string('telephone')->nullable();
            $table->boolean('block')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
