<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('title')->nullable();
            $table->enum('category', ['info', 'scholarship', 'jobs', 'event', 'volunteer']);
            $table->text('image')->nullable();
            $table->text('description')->nullable();
            $table->string('contact')->nullable();
            $table->string('link')->nullable();
            $table->string('user_id');
            $table->integer('viewer')->default(1);
            $table->boolean('publish')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
