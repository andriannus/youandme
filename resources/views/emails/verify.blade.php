@component('mail::message')

# Hallo, {{ $user->first_name }}

Selamat datang di Platform You & Me For Indonesia. <br>

Selangkah lagi untuk dapat berbagi kegiatan yang Anda miliki.. <br>

Silahkan klik link di bawah ini.

@component('mail::button', ['url' => url("verify", $user->verifyUser->token)])
Konfirmasi Email
@endcomponent

Terima kasih, <br>
{{ config('app.name') }}

@endcomponent