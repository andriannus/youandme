@component('mail::message')

# Hallo, {{ $user->first_name }}

Sepertinya Anda lupa sandi akun. Tenang saja, kami sudah membuat yang baru untuk Anda. <br>

@component('mail::panel')
Sandi akun Anda yang baru adalah <strong>{{ $pass_new }}</strong>
@endcomponent

Silahkan masuk ke dalam akun Anda dengan sandi yang baru.

Terima kasih, <br>
{{ config('app.name') }}

@endcomponent