import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        data: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null
    },

    mutations: {
        loginUser (state) {
            state.data = JSON.parse(localStorage.getItem('user'))
        },

        logoutUser (state) {
            localStorage.removeItem('default_auth_token')
            localStorage.removeItem('user')
            state.data = null
        }
    }
})