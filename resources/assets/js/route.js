import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from './components/Home'
import About from './components/About'
import AboutTeam from './components/AboutTeam'
import Contact from './components/Contact'
import MediaPartner from './components/MediaPartner'
import Header from './components/Header'
import Register from './components/Register'
import Login from './components/Login'
import NotFound from './components/NotFound'
import MyActivity from './components/MyActivity'
import ActivityForm from './components/ActivityForm'
import Search from './components/Search'
import ShowActivity from './components/ShowActivity'
import Profile from './components/Profile'
import SearchUser from './components/SearchUser'
import SearchActivity from './components/SearchActivity'
import UserActivity from './components/UserActivity'
import EditUser from './components/EditUser'
import PassReset from './components/PassReset'
import VerifyUser from './components/VerifyUser'
import ResendEmail from './components/ResendEmail'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes: [
		{
			path: '*',
			name: 'error404',
			component: NotFound
		},
		{
			path: '/login',
			name: 'login',
			component: Login,
			meta: {
				auth: false
			}
		},
		{
			path: '/register',
			name: 'register',
			component: Register,
			meta: {
				auth: false
			}
		},
		{
			path: '/verify/:token',
			name: 'verify-user',
			component: VerifyUser,
			meta: {
				auth: false
			}
		},
		{
			path: '/pass-reset',
			name: 'pass-reset',
			component: PassReset,
			meta: {
				auth: false
			}
		},
		{
			path: '/resend',
			name: 'resend',
			component: ResendEmail,
			meta: {
				auth: false
			}
		},
		{
			path: '/',
			component: Header,
			children: [
				{
					path: '',
					name: 'home',
					component: Home
				},
				{
					path: 'about-us',
					name: 'about-us',
					component: About
				},
				{
					path: 'about-us/team',
					name: 'about-team',
					component: AboutTeam
				},
				{
					path: 'contact-us',
					name: 'contact-us',
					component: Contact
				},
				{
					path: 'media-partner',
					name: 'media-partner',
					component: MediaPartner
				},
				{
					path: 'search',
					name: 'search',
					component: Search
				}
			]
		},
		{
			path: '/user',
			component: Header,
			children: [
				{
					path: 'edit',
					name: 'user-edit',
					component: EditUser,
					meta: {
						auth: true
					}
				},
				{
					path: 'search',
					name: 'user-search',
					component: SearchUser
				},
				{
					path: 'activity',
					name: 'user-activity',
					component: MyActivity,
					meta: {
						auth: true
					}
				}, 
				{
					path: ':id',
					name: 'user-profile',
					component: Profile
				},
				{
					path: ':id/activities',
					name: 'user-activities',
					component: UserActivity
				}
			]
		},
		{
			path: '/activity',
			component: Header,
			children: [
				{
					path: 'search',
					name: 'activity-search',
					component: SearchActivity
				},
				{
					path: 'add',
					name: 'activity-form',
					component: ActivityForm,
					meta: {
						auth: true
					},
					props: {
						edit: false
					}
				},
				{
					path: ':id',
					name: 'show-activity',
					component: ShowActivity
				}, 
				{
					path: ':id/edit',
					name: 'edit-activity-form',
					component: ActivityForm,
					meta: {
						auth: true
					},
					props: {
						edit: true
					}
				}
			]
		}
		
	],
	scrollBehavior(to, from, savedPosition) {
		return { x: 0, y: 0 }
	}
});

export default router