<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResendPass extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $pass_new;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user,$pass_new)
    {
        $this->user = $user;
        $this->pass_new = $pass_new;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Pulihkan Sandi')
                    ->markdown('emails.resend_pass');
    }
}
