<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    
    protected $fillable = [
        'id', 'first_name', 'last_name', 'email', 'password', 'avatar', 'gender', 'birthday', 'description', 'profession', 'agency', 'address', 'telephone', 'activity_id', 'block'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    public $incrementing = false;
    
    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function verifyUser()
    {
        return $this->hasOne('App\VerifyUser');
    }
}
