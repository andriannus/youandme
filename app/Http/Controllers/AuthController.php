<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

use JWTAuth;
use JWTAuthException;
use Hashids\Hashids;

use App\User;
use App\VerifyUser;
use App\Mail\VerifyMail;
use App\Mail\ResendPass;

class AuthController extends Controller
{
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$hashids = new Hashids(mt_rand(), 10);

		$id = $hashids->encode(1);
    	$first_name = $request->input('first_name');
        $last_name = $request->input('last_name');
    	$email = $request->input('email');
    	$password = $request->input('password');
        $birthday = $request->input('birthday');
    	$gender = $request->input('gender');
    	$profession = $request->input('profession');
    	$agency = $request->input('agency');

    	$user = new User([
			'id' => $id,
    		'first_name' => $first_name,
            'last_name' => $last_name,
    		'email' => $email,
    		'password' => bcrypt($password),
            'birthday' => $birthday,
    		'gender' => $gender,
    		'profession' => $profession,
    		'agency' => $agency,
		]);
		
		$verifyUser = new VerifyUser([
			'user_id' => $user->id,
			'token' => bcrypt($email),
		]);

    	if ($user->save()) {
			
			if ($verifyUser->save()) {
				$user->signin = [
					'href' => 'api/v1/user/signin',
					'method' => 'POST',
					'params' => 'email, password'
				];

				$response = [
					'success' => true,
					'message' => 'User Created',
					'data' => $user,
					'verified' => $verifyUser,
				];

				Mail::to($user->email)->send(new VerifyMail($user));

				return response()->json($response, 201);
			}

			$response = [
				'success' => false,
				'message' => 'Error during creation',
			];
	
			return response()->json($response, 404);
    	}

    	$response = [
    		'success' => false,
    		'message' => 'Error during creation',
    	];

    	return response()->json($response, 404);
    }

	/**
     * Login from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
		$email = $request->input('email');
		$password = $request->input('password');

		if ($user = User::with('verifyUser')->where('email', $email)->first()) {
			
			if ($user->block != 0) {
				$response = [
					'success' => false,
					'message' => 'User is blocked'
				];
		
				return response()->json($response, 403);

			} else {
				$credentials = [
					'email' => $email,
					'password' => $password
				];
	
				$token = null;
				try {
					if (!$token = JWTAuth::attempt($credentials)) {
						return response()->json([
							'success' => false,
							'message' => 'Password are incorrect',
						], 400);

					} else if ($user->verifyUser->verified != 1) {
						$response = [
							'success' => false,
							'message' => 'E-mail has not been verified'
						];
				
						return response()->json($response, 401);
					}
	
				} catch (JWTAuthException $e) {
					return response()->json([
						'message' => 'failed_to_create_token',
					], 404);
				}
	
				$response = [
					'success' => true,
					'message' => 'User sigin',
					'user' => $user,
					'token' => $token
				];
	
				return response()->json($response, 201)->header('Authorization', $token);
			}
		}

		$response = [
			'success' => false,
			'message' => 'Email has not been registered'
		];

		return response()->json($response, 404);
	}

	/**
     * Check current user from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function current(Request $request)
	{
		$id_user = $request->input('id_user');
		
		$current = User::with('verifyUser')->where('id', $id_user)->first();

		$response = [
			'success' => true,
			'message' => 'Current user',
			'data' => $current,
		];

		return response()->json($response, 200);
	}

	/**
     * Set active e-mail from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function active(Request $request)
	{
		$token = $request->query('token');
		$verify_user = VerifyUser::where('token', $token)->first();

		if ($verify_user->verified != 0) {
			$response = [
				'success' => true,
				'status' => true,
				'message' => 'User verified',
				'data' => $verify_user
			];

			return response()->json($response, 200);

		} else {
			$verify_user->verified = true;

			if (!$verify_user->update()) {
				$response = [
					'success' => false,
					'message' => 'Token not found'
				];
	
				return response()->json($response, 404);

			} else {
				$response = [
					'success' => true,
					'status' => false,
					'message' => 'Verify user updated',
					'data' => $verify_user
				];
	
				return response()->json($response, 201);
			}
		}
	}

	/**
     * Send new password from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function resend(Request $request)
	{
		$email = $request->input('email');
		$user = User::where('email', $email)->first();

		$pass_new = substr(md5(mt_rand()), 0, 8);
		$user->password = bcrypt($pass_new);

		if (!$user->update()) {
			$response = [
				'success' => false,
				'message' => 'Error during update password'
			];

			return response()->json($response, 404);
		} else {

			Mail::to($user->email)->send(new ResendPass($user, $pass_new));

			$response = [
				'success' => true,
				'message' => 'User Updated',
				'data' => $user
			];

			return response()->json($response, 200);
		}
	}

	/**
     * Resend e-mail verify from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function verify(Request $request)
	{
		$email = $request->input('email');
		$user = User::where('email', $email)->first();

		if (!$user) {
			$response = [
				'success' => false,
				'message' => 'Error during resend e-mail verify'
			];

			return response()->json($response, 404);
		} else {

			if ($user->verifyUser->verified != 0) {
				$response = [
					'success' => false,
					'message' => 'E-mail has been verified'
				];
	
				return response()->json($response, 403);
			} else {

				Mail::to($user->email)->send(new VerifyMail($user));

				$response = [
					'success' => true,
					'message' => 'E-mail sent',
					'data' => $user
				];

				return response()->json($response, 200);
			}
		}
	}

	/**
     * Check e-mail availability from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function checkMail(Request $request)
	{
		$email = $request->input('email');

		$user = User::where('email', $email)->first();

		if ($user) {
			return response()->json([
				'success' => true,
				'status' => false,
				'message' => 'Email has been used'
			]);
		} else {
			return response()->json([
				'success' => true,
				'status' => true,
				'message' => 'Email can be used'
			]);
		}
	}
	
	/**
     * Remove token from resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
	public function logout(Request $request)
	{
		$token = JWTAuth::getToken();
		
		JWTAuth::invalidate($token);

		$response = [
			'success' => true,
			'message' => 'User logut'
		];

		return response()->json($response, 200);
	}
}
