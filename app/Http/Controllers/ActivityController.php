<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use Image;
use Carbon\Carbon;
use Hashids\Hashids;

use App\Activity;
use App\Http\Resources\ActivityResource;
use App\Http\Resources\ActivitiesCollection;

class ActivityController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['index', 'show']]);
        $this->middleware('jwt.refresh', ['except' => ['index', 'show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = $request->query('query');
        $category = $request->query('category');
        $sort = $request->query('sort');

        // Not All
        if (!isset($query) && !isset($category) && !isset($sort)) {
            return new ActivitiesCollection(
                Activity::with('user')
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        }

        // Search only
        if (isset($query) && !isset($category) && !isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('title', 'like', '%'.$query.'%')
                        ->with('user')
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        }
        
        // Category only
        if (!isset($query) && isset($category) && !isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('category', $category)
                        ->with('user')
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        }
        
        // Sort only
        if (!isset($query) && !isset($category) && isset($sort)) {
            return new ActivitiesCollection(
                Activity::with('user')
                        ->orderBy('created_at', $sort)
                        ->paginate(5)
            );
        }

        // Search and category
        if (isset($query) && isset($category) && !isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('title', 'like', '%'.$query.'%')
                        ->where('category', $category)
                        ->with('user')
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        }

        // Search and sort
        if (isset($query) && !isset($category) && isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('title', 'like', '%'.$query.'%')
                        ->with('user')
                        ->orderBy('created_at', $sort)
                        ->paginate(5)
            );
        }

        // Category and sort
        if (!isset($query) && isset($category) && isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('category', $category)
                        ->with('user')
                        ->orderBy('created_at', $sort)
                        ->paginate(5)
            );
        }

        // All
        if (isset($query) && isset($category) && isset($sort)) {
            return new ActivitiesCollection(
                Activity::where('title', 'like', '%'.$query.'%')
                        ->where('category', $category)
                        ->with('user')
                        ->orderBy('created_at', $sort)
                        ->paginate(5)
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hashids = new Hashids(mt_rand(), 10);

        if ($request->input('image')) {
            $img_name = 'activity_'.Carbon::now()->format('dmY').'_'.rand(1, 99).'.jpeg';
            $id = $hashids->encode(1);
            $title = $request->input('title');
            $category = $request->input('category');
            $image = Image::make($request->input('image'));
            $description = $request->input('description');
            $contact = $request->input('contact');
            $link = $request->input('link');
            $viewer = $request->input('viewer');
            $user_id = $request->input('user_id');
            $publish = $request->input('publish');

            $activity = new Activity([
                'id' => $id,
                'title' => $title,
                'category' => $category,
                'image' => $img_name,
                'description' => $description,
                'contact' => $contact,
                'link' => $link,
                'viewer' => $viewer,
                'user_id' => $user_id,
                'publish' => $publish
            ]);

            if ($activity->save()) {
                $activity->user()->associate($user_id);
                Image::make($request->input('image'))
                    ->resize(600, null, function($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save('/home/youandme/public_html/images/activity/'.$img_name); 
                $activity->view_activity = [
                    'href' => 'api/v1/activity/'.$activity->id,
                    'method' => 'GET'
                ];
                $response = [
                    'success' => true,
                    'message' => 'Activity Created',
                    'data' => $activity
                ];
                return response()->json($response, 201);

            } else {
                $response = [
                    'success' => false,
                    'message' => 'Error during creation'
                ];

                return response()->json($response, 404);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $activity = Activity::with('user')->where('id', $id)->firstOrFail();

        $activity->view_activities = [
            'href' => 'api/v1/activity',
            'method' => 'GET'
        ];

        $response = [
            'success' => true,
            'message' => 'Activity Information',
            'data' => $activity
        ];

        return response()->json($response, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $path = pathinfo($request->input('image'));

        if (!isset($path['extension'])) {
            $title = $request->input('title');
            $category = $request->input('category');
            $image = 'activity_'.Carbon::now()->format('dmY').'_'.rand(1, 99).'.jpeg';
            $description = $request->input('description');
            $contact = $request->input('contact');
            $link = $request->input('link');
            $viewer = $request->input('viewer');
            $user_id = $request->input('user_id');
            $publish = $request->input('publish');

            $activity = Activity::with('user')->findOrFail($id);

            if (!$activity->user()->where('id', $user_id)->first()) {
                $response = [
                    'success' => false,
                    'message' => 'User not create the activity, update not successful',
                    'data' => $activity->user()->where('id', $user_id)->first()
                ];

                return response()->json($response, 401);
            } else {
                unlink('/home/youandme/public_html/activity/'.$activity->image);
                Image::make($request->input('image'))
                    ->resize(730, null, function($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->save(public_path('/images/activity/'.$image));

                $activity->title = $title;
                $activity->category = $category;
                $activity->image = $image;
                $activity->description = $description;
                $activity->contact = $contact;
                $activity->link = $link;
                $activity->viewer = $viewer;
                $activity->publish = $publish;

                if (!$activity->update()) {
                    $response = [
                        'success' => false,
                        'message' => 'Error during update'
                    ];

                    return response()->json($response, 404);
                } else {
                    $activity->view_activity = [
                        'href' => 'api/v1/activity/'.$activity->id,
                        'method' => 'GET'
                    ];

                    $response = [
                        'success' => true,
                        'message' => 'Activity Updated',
                        'data' => $activity
                    ];

                    return response()->json($response, 201);
                }
            }
        } else {
            $title = $request->input('title');
            $category = $request->input('category');
            $image = $request->input('image');
            $description = $request->input('description');
            $contact = $request->input('contact');
            $link = $request->input('link');
            $viewer = $request->input('viewer');
            $user_id = $request->input('user_id');
            $publish = $request->input('publish');

            $activity = Activity::with('user')->findOrFail($id);

            if (!$activity->user()->where('id', $user_id)->first()) {
                $response = [
                    'success' => false,
                    'message' => 'User not create the activity, update not successful',
                    'data' => $activity->user()->where('id', $user_id)->first()
                ];

                return response()->json($response, 401);

            } else {
                $activity->title = $title;
                $activity->category = $category;
                $activity->image = $image;
                $activity->description = $description;
                $activity->contact = $contact;
                $activity->link = $link;
                $activity->viewer = $viewer;
                $activity->publish = $publish;

                if (!$activity->update()) {
                    $response = [
                        'success' => false,
                        'message' => 'Error during update'
                    ];

                    return response()->json($response, 404);

                } else {
                    $activity->view_activity = [
                        'href' => 'api/v1/activity/'.$activity->id,
                        'method' => 'GET'
                    ];

                    $response = [
                        'success' => true,
                        'message' => 'Activity updated',
                        'data' => $activity
                    ];

                    return response()->json($response, 201);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::findOrFail($id);
        $user = $activity->user;
        $activity->user()->dissociate();

        if (!$activity->delete()) {
            $activity->user()->associate();

            $response = [
                'success' => false,
                'message' => 'Deletion failed'
            ];

            return response()->json($response, 200);

        } else {
            File::delete(public_path('/images/activity/'.$activity->image));
            
            $response = [
                'success' => true,
                'message' => 'Activity deleted',
                'create' => [
                    'href' => 'api/v1/activity',
                    'method' => 'POST',
                    'params' => 'title, category, description, contact, link, viewer, user_id, file_id, publish'
                ]
            ];

            return response()->json($response, 200);
        }
    }
}
