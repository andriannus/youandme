<?php

namespace App\Http\Controllers;

// Composer
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

use Image;
use Carbon\Carbon;

use App\User;
use App\Report;
use App\Http\Resources\UsersCollection;

use App\Activity;
use App\Http\Resources\ActivityResource;
use App\Http\Resources\ActivitiesCollection;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth', ['only' => 'update']); // JWT Auth
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
	{
        $query = $request->query('query');

        if (isset($query)) {
            return new UsersCollection(
                User::where('first_name', 'like', '%'.$query.'%')
                    ->orderBy('created_at', 'desc')
                    ->paginate(5)
            );
        } else {
            return new UsersCollection(
                User::orderBy('created_at', 'desc')
                    ->paginate(5)
            );
        }
	}

    /**
     * Display the specified resource.
     *
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
	public function show($id)
	{
        $user = User::with('activities')->where('id', $id)->firstOrFail();
        
        $user->view_users = [
            'href' => 'api/v1/user',
            'href' => 'GET'
        ];

        $response = [
            'success' => true,
            'message' => 'User Information',
            'data' => $user
        ];

        return response()->json($response, 200);
	}

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $q = $request->query('q');

        return new UsersCollection(
            User::where('first_name', 'like', '%'.$q.'%')->paginate(5)
        );
    }

    /**
     * Report the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function report(Request $request, $id)
    {
        $report = $request->input('report');

        $report = new Report([
            'user_id' => $id,
            'description' => $report
        ]);

        if ($report->save()) {
            $report->user()->associate($id);
            $report->view_user = [
                'href' => 'api/v1/user'.$id,
                'method' => 'GET'
            ];

            $response = [
                'success' => true,
                'message' => 'User reported',
                'data' => $report
            ];

            return response()->json($response, 201);
        } else {
            $response = [
                'success' => false,
                'message' => 'Error during reportion'
            ];

            return response()->json($response, 404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Apakah file yang dimasukkan memiliki ekstensi?
        $path = pathinfo($request->input('avatar'));

        if (!isset($path['extension'])) {
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $avatar = 'user_'.Carbon::now()->format('dmY').'_'.rand(1, 99).'.jpeg';
            $birthday = $request->input('birthday');
            $gender = $request->input('gender');
            $description = $request->input('description');
            $profession = $request->input('profession');
            $agency = $request->input('agency');
            $address = $request->input('address');
            $telephone = $request->input('telephone');

            $user = User::findOrFail($id);

            // Hapus avatar terlebih dahulu
            if ($user->avatar != 'avatar.jpeg') {
                unlink('/home/youandme/public_html/images/avatar/'.$user->avatar);
            }

            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->avatar = $avatar;
            $user->birthday = $birthday;
            $user->gender = $gender;
            $user->description = $description;
            $user->profession = $profession;
            $user->agency = $agency;
            $user->address = $address;
            $user->telephone = $telephone;

            if (!$user->update()) {
                $response = [
                    'success' => 'false',
                    'message' => 'Error during update'
                ];

                return response()->json($response, 404);
            } else {
                Image::make($request->input('avatar'))
                    ->fit(120)
                    ->save('/home/youandme/public_html/images/avatar/'.$avatar);

                $user->view_user = [
                    'href' => 'api/v1/user/'.$user->id,
                    'method' => 'GET'
                ];

                $response = [
                    'success' => true,
                    'message' => 'User Updated',
                    'data' => $user
                ];

                return response()->json($response, 201);
            }
        } else {
            $first_name = $request->input('first_name');
            $last_name = $request->input('last_name');
            $avatar = $request->input('avatar');
            $birthday = $request->input('birthday');
            $gender = $request->input('gender');
            $description = $request->input('description');
            $profession = $request->input('profession');
            $agency = $request->input('agency');
            $address = $request->input('address');
            $telephone = $request->input('telephone');

            $user = User::findOrFail($id);

            $user->first_name = $first_name;
            $user->last_name = $last_name;
            $user->avatar = $avatar;
            $user->birthday = $birthday;
            $user->gender = $gender;
            $user->description = $description;
            $user->profession = $profession;
            $user->agency = $agency;
            $user->address = $address;
            $user->telephone = $telephone;

            if (!$user->update()) {
                $response = [
                    'success' => 'false',
                    'message' => 'Error during update'
                ];

                return response()->json($response, 404);
            } else {
                $user->view_user = [
                    'href' => 'api/v1/user/'.$user->id,
                    'method' => 'GET'
                ];

                $response = [
                    'success' => true,
                    'message' => 'User Updated',
                    'data' => $user
                ];

                return response()->json($response, 201);
            }
        }
    }

    /**
     * Check old password the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function oldpass(Request $request, $id)
	{
		$password = $request->input('password');

		$user = User::findOrFail($id);
		$oldpass = $user->password;

		if (!Hash::check($password, $oldpass)) {
			$response = [
				'success' => false,
				'message' => 'Password not match'
			];
		} else {
			$response = [
				'success' => true,
				'message' => 'Password match'
			];
		}

		return response()->json($response, 200);
	}

    /**
     * Update password the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
	public function updatePass(Request $request, $id)
	{
		$password = $request->input('password');

		$user = User::findOrFail($id);

		$user->password = Hash::make($password);

		if (!$user->update()) {
			$response = [
				'success' => false,
				'message' => 'Error during update'
			];

			return response()->json($response, 404);
		} else {
			$user->view_user = [
				'href' => 'api/v1/user/'.$user->id,
				'method' => 'GET'
			];

			$response = [
				'success' => true,
				'message' => 'User Password Updated',
				'data' => $user
			];

			return response()->json($response, 201);
		}
	}

    /**
     * Display activities the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function activities(Request $request, $user_id)
    {
        $q = $request->query('q');

        if (isset($q)) {
            return new ActivitiesCollection(
                Activity::with('user')
                        ->where('user_id', $user_id)
                        ->where('title', 'like', '%'.$q.'%')
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        } else {
            return new ActivitiesCollection(
                Activity::with('user')
                        ->where('user_id', $user_id)
                        ->orderBy('created_at', 'desc')
                        ->paginate(5)
            );
        }
    }
}
