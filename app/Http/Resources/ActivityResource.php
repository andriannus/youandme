<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'category' => $this->category,
            'image' => $this->image,
            'description' => $this->description,
            'contact' => $this->contact,
            'link' => $this->link,
            'user_id' => $this->user_id,
            'viewer' => $this->viewer,
            'publish' => $this->publish,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'user' => $this->user,
            'view_activity' => [
                'href' => 'api/v1/activity/'.$this->id,
                'method' => 'GET'
            ],
        ];
    }
}
