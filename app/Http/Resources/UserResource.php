<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'email' => $this->email,
            'avatar' => $this->avatar,
            'gender' => $this->gender,
            'birthday' => $this->birthday,
            'description' => $this->description,
            'profession' => $this->profession,
            'agency' => $this->agency,
            'telephone' => $this->telephone,
            'last_login' => $this->last_login,
            'block' => $this->block,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'verify_user' => $this->verifyUser,
            'view_user' => [
                'href' => 'api/v1/user/'.$this->id,
                'method' => 'GET'
            ]
        ];
    }
}
