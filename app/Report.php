<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
    	'user_id', 'description'
    ];

    protected $hidden = [
    	'updated_at'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
