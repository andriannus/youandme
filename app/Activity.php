<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    public $incrementing = false;
    
    protected $fillable = [
    	'id', 'title', 'category', 'image', 'description', 'contact', 'link', 'user_id'
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
